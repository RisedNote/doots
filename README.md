# doots

## Getting started

this is only tested for **myslef**, I am using **Arch Linux**.
after downloading this repository install [**paru**](https://github.com/Morganamilo/paru) *an aur helper*. 
**the [*apps*](https://gitlab.com/RisedNote/doots/-/blob/main/apps) script uses paru**

## "apps" script

- make sure you are in the root of the `doots` directory

- the script should download all the applications I want for a base system

- then it will link each directory in their proper places *mainly the .config directories*

- the script will ask you if you want to link 3 specific files. After that, it is completed and you should **restart** your system
