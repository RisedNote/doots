local colorscheme = "tokyonight-moon" -- can be found in .local/share/nvim...packer/start/COLORSCHEME

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  return
end
