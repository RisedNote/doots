# Lines configured by zsh-newuser-install
#export QT_QPA_PLATFORMTHEME=qt5ct # moved to /etc/enviorment
export QT_QPA_PLATFORM=wayland

# icons = ~/.icons ; themes = /usr/share/themes 
# the value has to be the same name as the directory
#example: gsettings set org.gnome.desktop.interface <key> 'value'

gsettings set org.gnome.desktop.interface icon-theme 'Vimix-Ruby-dark'
gsettings set org.gnome.desktop.interface gtk-theme 'Colloid-Dark-Nord'
gsettings set org.gnome.desktop.interface cursor-theme Sunity-cursors
#gsettings set org.gnome.desktop.interface font-theme 'theme'

export STARSHIP_CONFIG=~/.config/starship/starship.toml

HISTFILE=~/.config/zsh/.histfile
HISTSIZE=3000
SAVEHIST=4000

bindkey -e
bindkey ";5D" backward-word
bindkey ";5C" forward-word

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
autoload -Uz compinit && compinit

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'
#zstyle :compinstall filename '/home/noterok/.config/zsh/.zshrc'
# End of lines added by compinstall

# alias
alias ls='ls --color'
alias rm='rm -i'
alias reboot='sudo reboot'
alias poweroff='sudo poweroff'
alias grep='grep --color=always'
alias sites='cd ~/Documents/hugo-sites/'
alias dmenu='rofi -dmenu --config .config/rofi/dmenu/type.rasi'
alias firefox='firefox-developer-edition'
alias nsxiv='nsxiv -a'

export LS_COLORS="*.pdf=0;33:*.zip=1;33:*.xz=1;33:*.gz=1;33:*.7z=1;33:*.mp4=0;35:*mp3=0;34:*ogg=0;34:*.mov=0;35:"

eval "$(starship init zsh)"
source ~/.config/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
